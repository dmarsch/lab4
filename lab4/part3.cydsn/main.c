/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
extern volatile int flag;
extern volatile float duty_cycle;
extern volatile int duty_current;
extern volatile int direction;
extern volatile int prev_state;

    int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    ENC_ISR_Start(); //enable interrupts
    CHG_DUTY_ISR_Start(); //enable interrupts

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    uint16 cnt;
    char display[17];
    char disp[17];
    float frequency;
    float t_count;
    float compare;
    Clock_1_Start();
    CNT_Start(); //start counter
    LCD_Start(); //start lcd screen
    duty_current = 0; //default 50% duty cycle
    
    cnt = 255; //period
    CNT_WritePeriod(cnt); //period register required for 1kHz as default frequency
    compare = 0.5*((float)cnt+ 1.0);
    CNT_WriteCompare(compare);
    t_count = 1.0/256000.0; //half of 10kHz clock frequency
    frequency = 1.0/((float)(cnt + 1.0)*t_count);
    duty_cycle = (100.0 * ((float)CNT_ReadCompare()/((float)CNT_ReadPeriod() + 1.0)));
    sprintf(display, "Freq: %.1f",frequency);
    LCD_PrintString(display);
    LCD_Position(0,14);
    LCD_PrintString("Hz");
    LCD_Position(1,0);
    sprintf(disp, "Duty: %.0f %%",duty_cycle);
    LCD_PrintString(disp);
    for(;;)
    {
        if (duty_current == 1) //button toggled to 25% duty cycle
        {
            compare = 0.25*((float)cnt +1.0);
            CNT_WriteCompare((uint16)compare);
            duty_cycle = (100.0 * ((float)CNT_ReadCompare()/((float)CNT_ReadPeriod() + 1.0)));
            LCD_Position(1,0);
            sprintf(disp, "Duty: %.0f %%",duty_cycle);
            LCD_PrintString(disp);
           
        }
        else if  (duty_current == 0) //button toggled to 50% duty cycle
        {
            compare = 0.5*((float)cnt +1.0);
            CNT_WriteCompare((uint16)compare);
            duty_cycle = (100.0 * ((float)CNT_ReadCompare()/((float)CNT_ReadPeriod() + 1.0)));
            LCD_Position(1,0);
            sprintf(disp, "Duty: %.0f %%",duty_cycle);
            LCD_PrintString(disp);
        }
        if (flag != 0 && direction == 0) //interrupt & cw turn to turn up frequency
        {
            flag = 0; //clear flag for next potential interrupt
            if (cnt <= 255) cnt = cnt -3;
            else if (cnt > 255) cnt = cnt -50;
            if (cnt >= 2559) cnt = 2559;
            if (cnt <= 25) cnt = 25;
            CNT_WritePeriod(cnt);
            if (duty_current == 1)  compare = 0.25*((float)cnt +1.0);
            if (duty_current == 0)  compare = 0.5*((float)cnt +1.0);
            CNT_WriteCompare((uint16)compare);
            frequency = 1.0/(((float)cnt + 1.0)*t_count);
            sprintf(display, "Freq: %.1f",frequency);
            LCD_ClearDisplay();
            LCD_Position(0,0);
            LCD_PrintString(display);
            LCD_Position(0,14);
            LCD_PrintString("Hz");
            LCD_Position(1,0);
            sprintf(disp, "Duty: %.0f %%",duty_cycle);
            LCD_PrintString(disp);
        }
        if (flag != 0 && direction == 1) //interrupt & ccw turn to turn down frequency
        {
            flag = 0; //clear flag for next potential interrupt
            if (cnt <= 255) cnt = cnt + 3;
            else if (cnt > 255) cnt = cnt + 50;
            if (cnt >=2559) cnt = 2559; //max divisor for min frequency
            if (cnt <= 25) cnt = 25; //min divisor for 10kHz max frequency
            CNT_WritePeriod(cnt);
            if (duty_current == 1)  compare = 0.25*((float)cnt +1.0);
            if (duty_current == 0)  compare = 0.5*((float)cnt +1.0);
            CNT_WriteCompare((uint16)compare);
            frequency = 1.0/(((float)cnt + 1.0)*t_count);
            sprintf(display, "Freq: %.1f",frequency);
            LCD_ClearDisplay();
            LCD_Position(0,0);
            LCD_PrintString(display);
            LCD_Position(0,14);
            LCD_PrintString("Hz");
            LCD_Position(1,0);
            sprintf(disp, "Duty: %.0f %%",duty_cycle);
            LCD_PrintString(disp);
        }                          
    } 
}
/* [] END OF FILE */
