/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
extern volatile int flag;
extern volatile int direction;
extern volatile int prev_state;

    int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    float cnt;
    char display[17];
    float frequency;
    LCD_Start(); //start lcd screen
    ENC_ISR_Start(); //enable interrupts
    cnt = 2400; //divisor
    frequency = 24000000/cnt;
    sprintf(display, "Freq: %.1f",frequency);
    LCD_PrintString(display);
    LCD_Position(0,14);
    LCD_PrintString("Hz");
    Clock_1_Start();
    Clock_1_SetDivider(cnt);
    for(;;)
    {
        if (flag != 0 && direction == 0) //interrupt & cw turn to turn up frequency
        {
            flag = 0; //clear flag for next potential interrupt
            if (cnt <= 2400) cnt = cnt -30;
            else if (cnt > 2400) cnt = cnt -400;
            if (cnt < 240) cnt = 240; //min divisor for max frequency
            if (cnt > 24000) cnt = 24000; //max divisor for min frequency
            Clock_1_SetDivider(cnt);
            frequency = 24000000/cnt;
            sprintf(display, "Freq: %.1f",frequency);
            LCD_ClearDisplay();
            LCD_Position(0,0);
            LCD_PrintString(display);
            LCD_Position(0,14);
            LCD_PrintString("Hz");
        }
        if (flag != 0 && direction == 1) //interrupt & ccw turn to turn down frequency
        {
            flag = 0; //clear flag for next potential interrupt
            if (cnt <= 2400) cnt = cnt + 30;
            else if (cnt > 2400) cnt = cnt + 400;
            if (cnt > 24000) cnt = 24000; //max divisor for min frequency
            if (cnt < 240) cnt = 240; //min divisor for max frequency
            Clock_1_SetDivider(cnt);
            frequency = 24000000/cnt;
            sprintf(display, "Freq: %.1f",frequency);
            LCD_ClearDisplay();
            LCD_Position(0,0);
            LCD_PrintString(display);
            LCD_Position(0,14);
            LCD_PrintString("Hz");
        }                          
    }          
}
/* [] END OF FILE */
